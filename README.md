PATCHWORK | INSTRUÇÕES
=========
# * GIT
## Clonando o repositório do projeto

```bash
git clone [url]
```

# * GIT ANNEX
## Configurando repositório do git annex
Comece com

```bash
#| Entra no diretório do projeto
cd patchwork
#| Inicia repositorio do git annex
git annex init
#| Autentica com sua conta do GoogleDrive
git-annex-remote-googledrive setup
#| Siga as instruções
```

Depois, faça

```bash
git annex initremote google \
	type=external externaltype=googledrive \
	prefix=patchwork.git-annex \
	chunk=0 \
	encryption=shared mac=HMACSHA512
```

o que pode demorar um pouco. É útil também fazer

```bash
git config annex.backend SHA256
```

mas só se você for modificar o anexo adicionando novos arquivos a ele.
`SHA256` faz com que os arquivos sejam indexados em função dos seus conteúdos e não dos nomes.

## Clonando o repositório em uma nova máquina

Depois de clonar o repositório, basta repetir o passo

```bash
git-annex-remote-googledrive setup
```

e seguir as instruções, como antes, e então rodar

```bash
git annex enableremote google
```

## Sincronizando o repositório

Para copiar todos os arquivos remotos para o diretório local e vice-versa, use

```bash
git annex sync --content
```

Se você só quiser sincronizar apenas os meta-dados (organização e nomes dos arquivos sem fazer donwload ou upload), basta remover a flag `--content`.

## Copiando arquivos

#### Local >> Remoto
Para copiar arquivos específicos para o repositório remoto, use

```bash
git annex copy <arquivo_ou_pasta> --to google
```
#### Local << Remoto
Para copiar um arquivo do remoto para a máquina local, use

```bash
git annex get <arquivo_ou_pasta>
```

## Adicionando arquivos no repositorio

```bash
#|Adicione o arquivo
git annex add <arquivo_ou_diretorio>
#|Sincronize o repositorio
git annex sync --content
```

## Removendo arquivos do repositorio

```bash
#|Remova o arquivo da márina
rm -f <arquivo_ou_diretorio>
#|Sincronize o repositorio
git annex sync --content
```
Isso pode deixar lixo nos repositorios que contenham uma cópia do arquivo deletado. Então
```bash
#|Lista arquivos no git annex que não sao usados em nenhum branch
git annex unused
#|Deleta um range de arquivos no git annex
git annex dropunused 1-10
```

ou

```bash
#|Vai deletar todos as copias dos arquivos
git annex unused --force
```

Adicionando a flag `--from <remotename>` é possível remover arquivos de repositórios específicos


## Metodo para deletar todas a copias de arquivos

Como o gitannex por padrão evita duplicatas de arquivos, deletar um arquivo implica em deletar todas as suas duplicadas

```bash
#|deleta o arquivo do git annex
git annex drop --force <file>
#|lista em que repositorios o arquivo tem copias
git annex whereis <file>
#|repetir essa etapa para cada repositorio listado na etapa anterior
git annex drop --force <file> --from <repo>
#|deleta o arquivo
rm file
#|sincroniza repositorio local com remoto
git annex sync
#
```



